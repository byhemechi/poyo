import 'dart:math';
import 'dart:typed_data';

/// Board class, handles most things to do with puyos
class Board {
  Uint8List data = Uint8List(6 * 13);

  Random puyoGenerator = Random();

  int _frame = 0;

  Uint8List incomingPuyos;

  /// Takes 2 optional named values: a `Uint8list` of board data and an instance of `Random` for generating drops
  Board({this.data, this.puyoGenerator});

  int _getIndex(int x, int y) {
    if (x > 5 || x < 0) throw RangeError.range(x, 0, 5);
    if (y > 12 || y < 0) throw RangeError.range(y, 0, 12);
    return y * 6 + x;
  }

  /// Gets the puyo at cell (x, y)
  int get(int x, int y) => data[_getIndex(x, y)];

  /// Sets the puyo at cell (x, y)
  void set(x, y, int value) {
    if (value < 0 || value > 5) throw RangeError.range(value, 0, 5);
    data[_getIndex(x, y)] = value;
  }

  /// Shuffles the puyos down. Returns false if nothing falls;
  bool applyGravity() {
    bool found = false;
    for (int i = 6 * 13 - 1; i > 1; ++i) {
      int currentRow = i ~/ 6;
      int currentCol = i - currentRow * 6;
      int pointAbove = get(currentCol, currentRow - 1);
      if (data[i] == 0 && pointAbove != 0) {
        data[i] = pointAbove;
        set(currentCol, currentRow - 1, 0);
        found = true;
      }
    }
    return found;
  }

  /// If the game has been lost (Puyo in the way of the drop)
  bool get hasLost => get(3, 1) != 0;

  /// A single game tick, usually runs every ~30ms or so
  void tick() {
    if (incomingPuyos.length < 8) {
      for (int i = 0; i < 8 - incomingPuyos.length; ++i)
        incomingPuyos.add(puyoGenerator.nextInt(0xff));
    }

    _frame++;
  }

  // TODO: groups
  getGroupAt() {}
}
