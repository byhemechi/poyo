import 'dart:async';

enum PuyoInput {
  rotateClock,
  rotateAntiClock,
  drop,
}

class _InputHandler {
  _InputHandler();
  List _inputList = [];

  // Marks an input as "pressed"
  void add(PuyoInput i) {
    if (!_inputList.contains(i)) _inputList.add(i);
  }

  // Marks an input as "pressed"
  void remove(PuyoInput i) {
    if (_inputList.contains(i)) _inputList.remove(i);
  }
}

// The base game
class PuyoGame {
  Stream<int> get state {}
  _InputHandler input = _InputHandler();
}
