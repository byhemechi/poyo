# Poyo Engine

## A barebones engine for puyo puyo

The whole idea of poyo engine is that it has no frontend, and it has no server.
This means that it can be implemented in a way that only the input and multiplayer components need to be written, and the game code can be identical everywhere.
